//
//  Color extension.swift
//  LocalizationManager
//
//  Created by Permyakov Vladislav on 11.04.2022.
//

import Foundation
import SwiftUI

extension Color{
    static let mintWhite = Color("white")
    static let darkSlateGreen = Color("darkGreen")
    static let lightSeaGreen = Color("lightGreen")
    static let darkSeaGreen = Color("mediumGreen")
    static let lightCoral = Color("pink")
}
