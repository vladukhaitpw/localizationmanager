//
//  LocalizationManagerApp.swift
//  LocalizationManager
//
//  Created by Permyakov Vladislav on 08.04.2022.
//

import SwiftUI


let widthConst: CGFloat = 300
let heightConst: CGFloat = 60
@main
struct LocalizationManagerApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
                .preferredColorScheme(.dark)
        }
    }
}
